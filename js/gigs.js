"use strict";

//TODO: request your own API key from https://www.songkick.com/api_key_requests/new
const skKey = "PasteAPIKeyHere";
//TODO: specify an artist ID
const artistId = "7011564";
const skUpcoming =
  "https://api.songkick.com/api/3.0/artists/" + artistId + "/calendar.json?apikey=";
const skPast =
  "https://api.songkick.com/api/3.0/artists/" + artistId + "/gigography.json?apikey=";

const dateConvert = dateString => {
  const months = [
    "JAN",
    "FEB",
    "MAR",
    "APR",
    "MAY",
    "JUN",
    "JUL",
    "AUG",
    "SEP",
    "OCT",
    "NOV",
    "DEC"
  ];
  let dateObj = new Date(Date.parse(dateString));
  return (
    dateObj.getDate() +
    " " +
    months[dateObj.getMonth()] +
    " " +
    dateObj.getFullYear()
  );
};

const getGigs = (url, theDiv, oldToNew = true) => {
  $.getJSON(url, function (data) {
    var items = [];
    $.each(data.resultsPage.results.event, function (index, gig) {

      let range =
        "<div class='date-range sk-left-2' itemprop='startDate' content='" + gig.start.date + "'>" + dateConvert(gig.start.date);
      if (undefined !== gig["end"]) {
        range = range + " - " + dateConvert(gig.end.date);
      }
      range = range + "</div>";

      const name = "<div class='sk-left-2'>" + gig.displayName + "</div>";
      const location = "<span>" + gig.location.city + "</span>";

      const skEvent =
        "<a itemprop='url' href=" +
        gig.uri +
        " target=_blank> <img class='sk-badge' src='img/sk-badge-black.svg' alt='Event on Songkick'></img> </a>";
      const googleMaps =
        "<a href='https://maps.google.com/?q=" +
        +gig.location.lat +
        "," +
        gig.location.lng +
        "' target=_blank> <img class='google-maps' src='img/google-maps.svg' alt='Venue on Google Maps'></img> </a>";

      items.push(
        "<hr><div id='" +
        index +
        "'>" +
        range +
        skEvent +
        name +
        location +
        googleMaps +
        "</div>"
      );
    });

    if (!oldToNew) {
      items = items.reverse();
    }
    $("<div/>", {
      html: items.join("")
    }).appendTo("#" + theDiv);

  }).fail(function () {
    $("<div/>", {
      html: "Unable to load gigs. Refresh page to try again."
    }).appendTo("#" + theDiv);
  });
};

// PROD //////
// getGigs(skUpcoming + skKey, "upcoming");
// getGigs(skPast + skKey, "past", false);
// PROD //////


// DEV //////
// local json
console.log("Local files loading...");
getGigs("dev/gigs.json", "upcoming");
getGigs("dev/past-gigs.json", "past", false);
// break it
//getGigs("doesntexist.json", "upcoming", true);
// DEV //////