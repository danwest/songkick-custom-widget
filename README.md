# Songkick Custom Widget

> Get and display an artist's gigs on your website

A widget created using the Songkick API to get and display upcoming and past gigs for an artist. This is an alternative to the official widget, which I've found improves on SEO. Also added a Google maps link for the event venue.
An example of it on a site I worked on - seazooband.com

# Installation

- [Visual Studio Code](https://code.visualstudio.com/)
- [Request a Songkick API Key](https://www.songkick.com/api_key_requests/new)

# Meta

Dan West - danwest2805@gmail.com

Distributed under the MIT license. See [LICENSE.txt](https://gitlab.com/danwest/songkick-custom-widget/blob/master/LICENSE.txt) for more information.

https://gitlab.com/danwest/songkick-custom-widget
